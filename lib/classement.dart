import 'package:flutter/material.dart';
import 'package:meetballl/main.dart';
import 'footer.dart';

class Classement extends StatefulWidget {
  @override
  _ClassementState createState() => _ClassementState();
}

class _ClassementState extends State<Classement> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: back,
      appBar: AppBar(
        centerTitle: true,
        title: Text("Classement", textAlign: TextAlign.center),
        backgroundColor: Colors.indigo,
      ),
      persistentFooterButtons: <Widget>[
        Footer(),
      ],
      body: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.4,
            child: OutlinedButton(
              style: OutlinedButton.styleFrom(
                primary: Colors.black,
              ),
              onPressed: () {},
              child: Text(
                "Rencontre",
                style: TextStyle(fontSize: 20),
              ),
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.4,
            child: OutlinedButton(
              style: OutlinedButton.styleFrom(
                primary: Colors.black,
              ),
              onPressed: () {},
              child: Text(
                "Notes",
                style: TextStyle(fontSize: 20),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
