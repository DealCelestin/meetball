import 'dart:convert';
import 'dart:core';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server/gmail.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class TerrainModel extends Model {
  var vaDataTerrain = [];
  var inDataTerrainDev = [];
  int tailleTerrain = 0;
  int tailleTerrainDev = 0;
  bool afficher = false;
  bool afficherDev = false;
  bool nomVerifier = true;
  List<String> terrain_selectionner = List();

  Future<String> terrain() async {
    // pour la recherche des terrains nous avons ici la list de tout les terrain visible par les utilisateurs
    var url = 'https://meetball.fr/www/get_terrain.php';
    http.Response response = await http.get(url);
    var data = jsonDecode(response.body);
    vaDataTerrain = data;
    tailleTerrain = data.length;
    afficher = true;
    notifyListeners();
    return " fin de fonction";
  }

  Future<String> terrainDev() async {
    var url = 'https://meetball.fr/www/get_terrain_dev.php';
    http.Response response = await http.get(url);
    var data = jsonDecode(response.body);
    inDataTerrainDev = data;
    tailleTerrainDev = data.length;
    notifyListeners();
    return " fin de fonction";
  }

  Future<String> verificationNom(String nomEcrit) async {
    var url = 'https://meetball.fr/www/get_terrain_nom.php';
    http.Response response = await http.get(url);
    var data = jsonDecode(response.body);

    nomVerifier = true;
    bool obtimisation = false;
    int nombre = data.length;
    for (var i = 0; i < nombre && obtimisation == false; i++) {
      if (nomEcrit == data[i]) {
        nomVerifier = false;
        obtimisation = true;
      }
    }
    return " fin de fonction";
  }

  Future<String> ajouterTerrain(
      String nom,
      String adresse,
      String ville,
      int nombreTerrain,
      String image1,
      String image2,
      String image3,
      String image4,
      String sol,
      String ouverture,
      String urlgoogle,
      String urlwaze,
      String groupe,
      String mail,
      String prennom) async {
    var url = 'https://meetball.fr/www/post_terrain.php';
    String json =
        '{"nom":"$nom","adresse":"$adresse","ville":"$ville","nombre_terrain":"$nombreTerrain","image1":"$image1","image2":"$image2","image3":"$image3","image4":"$image4","sol":"$sol","ouverture":"$ouverture","urlgoogle":"$urlgoogle","urlwaze":"$urlwaze","groupe":"$groupe"}'; // make POST request
    print(json);
    Response response = await post(url, body: json);

    String body = response.body;
    print("reponse");
    print(body);
    // ignore: deprecated_member_use
    List reponsee = List();
    if (body.isNotEmpty) {
      reponsee = jsonDecode(body);
    }

    String lienimage1 = " ";
    String lienimage2 = " ";
    String lienimage3 = " ";
    String lienimage4 = " ";
    for (var i = 0; i < reponsee.length; i++) {
      switch (i) {
        case 0:
          lienimage1 = reponsee[0].toString();
          break;
        case 1:
          lienimage2 = reponsee[1].toString();
          break;
        case 2:
          lienimage3 = reponsee[2].toString();
          break;
        case 3:
          lienimage4 = reponsee[3].toString();
          break;
        default:
      }
    }

    String username = 'equipemeetball@gmail.com';
    String password = 'Projet1*';

    final smtpServer = gmail(username, password);
    // Use the SmtpServer class to configure an SMTP server:
    // final smtpServer = SmtpServer('smtp.domain.com');
    // See the named arguments of SmtpServer for further configuration
    // options.

    // Create our message.
    final message = Message()
      ..from = Address(username, 'équipe Meetball')
      ..recipients.add('equipemeetball@gmail.com')
      // ..bccRecipients.add(Address('bccAddress@example.com'))
      ..subject = 'Nouveau terrain'
      ..text = 'This is the plain text.\nThis is line 2 of the text part.'
      ..html =
          "<h1>Nouveau terrain</h1>\n<p> $nom </p>\n<p>  $adresse </p>\n<p>  $ville  </p>\n<p> $nombreTerrain </p>\n<p> $sol </p>\n<p> $ouverture </p>\n<p> $urlgoogle </p>\n<p> $urlwaze  </p>\n<p> $groupe  </p>\n<p> $lienimage1 </p>\n<p>$lienimage2</p>\n<p>$lienimage3</p>\n<p>$lienimage4 </p>";
    await send(message, smtpServer);

    final message2 = Message()
      ..from = Address(username, 'équipe Meetball')
      ..recipients.add(mail)
      // ..bccRecipients.add(Address('bccAddress@example.com'))
      ..subject = 'Nouvelle avis Meetball'
      ..html =
          "<h1>Bonjour $prennom</h1>\n<p>Votre ajout de terrain a bien été pris en compte, nous vous remercions de votre soutien, nous procédons à quelques vérifications afin de pouvoir valider définitivement ce terrain.</p>\n<p> A très bientôt sur les Playground. </p>\n<p>  Chloé de Meetball </p>";
    await send(message2, smtpServer);

    return body;
  }

  Future<String> terrainselectionner(
      String nom,
      String adresse,
      String ville,
      String nombreTerrain,
      String image1,
      String image2,
      String image3,
      String image4,
      String sol,
      String ouverture,
      String commentaire,
      String urlgoogle,
      String urlwaze) {
    if (terrain_selectionner.isNotEmpty) {
      terrain_selectionner.clear();
    }

    terrain_selectionner.add(nom);
    terrain_selectionner.add(adresse);
    terrain_selectionner.add(ville);
    terrain_selectionner.add(nombreTerrain);
    terrain_selectionner.add(image1);
    terrain_selectionner.add(image2);
    terrain_selectionner.add(image3);
    terrain_selectionner.add(image4);
    terrain_selectionner.add(sol);
    terrain_selectionner.add(ouverture);
    terrain_selectionner.add(urlgoogle);
    terrain_selectionner.add(urlwaze);
    terrain_selectionner.add(commentaire);
  }
}
