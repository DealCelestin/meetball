import 'dart:convert';
import 'dart:core';
import 'package:meetballl/models/Model_img.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

// ici on vas gerer tous les données des differente ville
class VilleModel extends Model {
  String ville_choisi = "";
  List data_ville = List();

  Future<List> lieu_ville() async {
    var url = 'https://meetball.fr/www/post_lieu_ville.php';
    String json = '{"ville":"$ville_choisi"}'; // make POST request

    Response response = await post(url, body: json);
    List data = jsonDecode(response.body);

    return data;
  }

  Future<List> ville() async {
    var url = 'https://meetball.fr/www/get_ville.php';
    http.Response response = await http.get(url);
    data_ville = jsonDecode(response.body);
  }
}
