import 'dart:convert';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server/gmail.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class GameModel extends Model {
  var vaDataGame = [];

  int taille = 0;

  var vaRencontreVisualiser;
  bool afficher = false;
  bool boAfficherLieu = false;
  List lisCommentaire = [];

  var vaAdresseLieu;
  var vaUrlLieu;
  var vaUrlwazeLieu;
  var vaCommentaireLieu;
  var vaNomLieu;
  var vavilleLieu;
  var vaIdTerrain;
  String terrainrencontre = "";
  int nombre = 0;
  int mmax = 20;
  int lastmmax = 0;
  // variable de sélection des la rencontre pour la page profil rencontre
  String lieu = "";
  String inIdRencontre;
  int nombJoueur;
  String daterencontre = " ";
  String heurerencontre = " ";
//pour le calendar

  Map<DateTime, List> events = {};
  final selectedDay = DateTime.now();

  // variable pour rajouter un match
  String date = "Date";
  String time = "Heure";
// variable pour les commentaire

  void initState() {}

  Future<String> match() async {
    var url = 'https://meetball.fr/www/get_match_future.php';
    http.Response response = await http.get(url);
    var data = jsonDecode(response.body);
    vaDataGame = data;
    taille = data.length;
    afficher = true;
    notifyListeners();
    return " fin de fonction";
  }

  Future<String> matchCalendar() async {
    events.clear();
    var url = 'https://meetball.fr/www/get_match.php';
    http.Response response = await http.get(url);
    final data = jsonDecode(response.body);

    for (var i = 0; i < data.length; i++) {
      String ok = "}" + data[i]['jours'].toString() + "/";
      int jour = int.parse(ok.split('}')[1].split('-')[0]);
      int mois = int.parse(ok.split('-')[1].split('-')[0]);
      String placement = jour.toString() + '-' + mois.toString() + '-';
      int ans = int.parse(ok.split(placement)[1].split('/')[0]);
      final _selectedDay = DateTime.utc(ans, mois, jour, 20, 18, 00);

      List<dynamic> valider = [];
      valider.add(data[i]);
      if (events[_selectedDay] != null) {
        events.update(_selectedDay, (list) => list..add(data[i]));
      } else {
        events.addEntries([
          MapEntry(_selectedDay, valider),
        ]);
      }
    }
    notifyListeners();
    return " fin de fonction";
  }

  Future<String> ajoutMatch(
      String lieu,
      String date,
      String heure,
      String strinNombreJoueur,
      String pseudo,
      int nombre_match,
      String mail,
      String prenom) async {
    afficher = false;
    String url = 'https://meetball.fr/www/post_match.php';
    String json =
        '{"lieu":"$lieu","date":"$date","heure":"$heure","nombre_joueur":"$strinNombreJoueur","pseudo":"$pseudo","nombre_match":"$nombre_match"}'; // make POST request
    print(json);
    Response response = await post(url, body: json);
    String body = response.body;

    // String username = 'equipemeetball@gmail.com';
    // String password = 'Projet1*';

    // final smtpServer = gmail(username, password);

    // // Use the SmtpServer class to configure an SMTP server:
    // // final smtpServer = SmtpServer('smtp.domain.com');
    // // See the named arguments of SmtpServer for further configuration
    // // options.

    // // Create our message.
    // final message = Message()
    //   ..from = Address(username, 'équipe Meetball')
    //   ..recipients.add(mail)
    //   // ..bccRecipients.add(Address('bccAddress@example.com'))
    //   ..subject = 'Nouveau terrain'
    //   ..text = 'This is the plain text.\nThis is line 2 of the text part.'
    //   ..html =
    //       "<h1>Bonjour $prenom</h1>\n<p>Vous avez proposé une nouvelle rencontre le $date à $heure sur le terrain $lieu.</p>\n<p> Patientez, vos adversaires arrivent. </p>\n<p>Chloé de Meetball </p>";
    // await send(message, smtpServer);
    return body;
  }

  Future<String> terrain() async {
    var url = 'https://meetball.fr/www/get_terrain.php';
    http.Response response = await http.get(url);
    var data = jsonDecode(response.body);
    bool validation = true;
    int inNombreTour = data.length;
    int n = 0;
    while (inNombreTour > n && validation == true) {
      if ((data[n]['nom'].toString() + " ") == lieu.toString()) {
        vaAdresseLieu = data[n]['adresse'];
        vaUrlLieu = data[n]['url'];
        vaUrlwazeLieu = data[n]['urlwaze'];
        vaCommentaireLieu = data[n]['commentaire'];
        vaNomLieu = data[n]['ville'];
        vavilleLieu = data[n]['nom_t'];
        vaIdTerrain = data[n]['id'];

        validation = false;
      }
      if ((data[n]['nom'].toString()) == lieu.toString()) {
        vaAdresseLieu = data[n]['adresse'];
        vaCommentaireLieu = data[n]['commentaire'];
        vaNomLieu = data[n]['ville'];
        vavilleLieu = data[n]['nom_t'];
        vaIdTerrain = data[n]['id'];
        vaUrlLieu = data[n]['url'];
        vaUrlwazeLieu = data[n]['urlwaze'];

        validation = false;
      }
      n++;
    }
    boAfficherLieu = true;
    notifyListeners();
    return " fin de fonction";
  }

  participation(
    int inIdRencontre,
    String pseudo,
    int inNomInviter,
  ) async {
    // ajouter la participation d'une personne à une rencontre
    String url = 'https://meetball.fr/www/post_participation.php';
    String json =
        '{"id_rencontre":"$inIdRencontre","nombre_joueur":"$inNomInviter","pseudo":"$pseudo"}'; // make POST request
    Response response = await post(url, body: json);
    String body = response.body;
    return body;
  }

  commentaire() async {
    var url = 'https://meetball.fr/www/post_commentaire_rencontre.php';
    String json =
        '{"id_rencontre":"$inIdRencontre","date":"0"}'; // make POST request

    if (lisCommentaire.isNotEmpty) {
      String date = lisCommentaire[lisCommentaire.length - 1]['date'];
      json = '{"id_rencontre":"$inIdRencontre","date":"$date"}';
    }
    Response response = await post(url, body: json);
    var data;
    if (response.body.isNotEmpty) {
      data = jsonDecode(response.body);
    }
    List list = data as List;
    // on veux savoir des nouveaux commentaires

    // si il y as un nouveau message || l'on doit affiche plus de message
    if (list.length > nombre || mmax > lastmmax) {
      lastmmax = mmax;
      lisCommentaire.clear();
      var vaNombreTour = list.length;
      int n = 0;
      while (vaNombreTour > n) {
        // if ((list[n]['stirnIdrencontre'].toString()) == inIdRencontre && (vaNombreTour - n) < mmax) {
        // avant il y avais cette condition mais je comprent pas comment elle fonctionne
        if ((list[n]['id_rencontre'].toString()) == inIdRencontre &&
            (vaNombreTour - n) < mmax) {
          lisCommentaire.add(list[n]);
        }
        n++;
      }
      // si il n'y a pas de commentaire on n'affiche pas les container avec les commentaire
      nombre = n;
      notifyListeners();
      // on déclanche le nouveau scroll en bas que si c'est pour un nouveau message et pas pour le nouvelle affichage des messages avec les plus vieux
      if (mmax < lastmmax) {
        //afficher un truc pour dire qu'il y as un nouveaux message

      } else {
        //
      }
    }

    return " fin de fonction";
  }

  ajouterCommentaire(String commentaire, String pseudo) async {
    String url = 'https://meetball.fr/www/post_commentaire.php';
    String json =
        '{"id_rencontre":"$inIdRencontre","commentaire":"$commentaire","pseudo":"$pseudo"}'; // make POST request
    Response response = await post(url, body: json);
    String body = response.body;
    return body;
  }

  supParticipation(int stirnIdrencontre, String pseudo, int nomInviter) async {
    // supprimer la participation d'une personne à une rencontre
    String url = 'https://meetball.fr/www/post_sup_participation.php';

    String json =
        '{"id_rencontre":"$stirnIdrencontre","pseudo":"$pseudo","inviter":"$nomInviter"}'; // make POST request
    await post(url, body: json);
  }

  supCommentaire(
    int id,
  ) async {
    String url = 'https://meetball.fr/www/post_sup_commentaire.php';

    String json = '{"id":"$id"}'; // make POST request
    Response response = await post(url, body: json);
    String body = response.body;
    return body;
  }

  initdate(String newdate, String newtime) async {
    date = newdate;
    time = newtime;
    notifyListeners();
    return;
  }
}
