import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:meetballl/footer.dart';
import 'package:meetballl/main.dart';
import 'package:meetballl/models/Model_img.dart';
import 'package:meetballl/models/Model_match.dart';
import 'package:meetballl/models/Model_terrain.dart';
import 'package:photo_view/photo_view.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:url_launcher/url_launcher.dart';

double ditanceCourt = 1000000;
String tkt = "location";
List lieupro = [];
bool chnagecouleur = false;

class TerrainPro extends StatefulWidget {
  @override
  _TerrainProState createState() => _TerrainProState();
}

double lat2 = 0;
double lon2 = 0;
double lon1;
double lat1;

class _TerrainProState extends State<TerrainPro> {
  @override
  Widget build(BuildContext context) {
    terrain() async {
      List contruction = [];
      var location = new Location();
      var currentLocation = await location.getLocation();
      lat1 = currentLocation.latitude;
      lon1 = currentLocation.longitude;
      for (var i = 0;
          i < ScopedModel.of<TerrainModel>(context).tailleTerrain;
          i++) {
        String lieu = "";
        lieu = ScopedModel.of<TerrainModel>(context)
            .vaDataTerrain[i]['url']
            .toString();
        if (await canLaunch(lieu)) {
          String valueString =
              "/" + lieu.toString().split('@')[1].split('/')[0];
          int nombrecar = valueString.length;
          String valuelieu = valueString.substring(0, nombrecar - 1);
          lat2 = double.parse(valuelieu.toString().split('/')[1].split(',')[0]);
          lon2 = double.parse(valuelieu.toString().split(',')[1]);
          double distance = 6371 *
              acos((sin(lat1 * (pi / 180)) * sin(lat2 * (pi / 180))) +
                  (cos(lat1 * (pi / 180)) *
                      cos(lat2 * (pi / 180)) *
                      cos(lon1 * (pi / 180) - lon2 * (pi / 180))));

          Map tkt = {
            'contruiction':
                ScopedModel.of<TerrainModel>(context).vaDataTerrain[i],
            "distance": distance
          };
          contruction.add(tkt);
        }
      }
      int copie = contruction.length;
// objtif classer les lieu dans l'ordre
      for (var i = 0; i < copie; i++) {
        double nombreplus = 10000;
        int place;
        for (var n = 0; n < contruction.length; n++) {
          if (contruction[n]['distance'] <= nombreplus) {
            nombreplus = contruction[n]['distance'];
            place = n;
          }
        }
        lieupro.add(contruction[place]);
        contruction.removeAt(place);
      }
      return lieupro;
    }

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Playground à coté"),
          backgroundColor: Colors.indigo,
        ),
        persistentFooterButtons: <Widget>[
          Footer(),
        ],
        backgroundColor: back,
        body: FutureBuilder<dynamic>(
          future: terrain(),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              return AffImage();
            } else {
              return Center(
                child: SizedBox(
                  child: CircularProgressIndicator(),
                  width: 60,
                  height: 60,
                ),
              );
            }
          },
        ));
  }
}

class AffImage extends StatefulWidget {
  @override
  _AffImageState createState() => _AffImageState();
}

class _AffImageState extends State<AffImage> {
  Widget build(BuildContext context) {
    return ScopedModelDescendant<ImgModel>(builder: (context, child, img) {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: 3,
            itemBuilder: (context, i) {
              //  tous les vzriable du lieu sont dans lieupro[i]['contruiction']
              List<String> groupe = ["", "", "", ""];
              int inNombreTour = 0;
              while (img.inTailleImg > inNombreTour) {
                if (lieupro[i]['contruiction']['id'] ==
                    img.vaDataImg[inNombreTour]["id_lieu"]) {
                  if (groupe[0] == "") {
                    groupe[0] = img.vaDataImg[inNombreTour]["lien"];
                  } else if (groupe[1] == "") {
                    groupe[1] = img.vaDataImg[inNombreTour]["lien"];
                  } else if (groupe[2] == "") {
                    groupe[2] = img.vaDataImg[inNombreTour]["lien"];
                  } else if (groupe[3] == "") {
                    groupe[3] = img.vaDataImg[inNombreTour]["lien"];
                  }
                }
                inNombreTour++;
              }

              return GestureDetector(
                onTap: () {
                  ScopedModel.of<TerrainModel>(context).terrainselectionner(
                    lieupro[i]['contruiction']['nom'],
                    lieupro[i]['contruiction']['adresse'],
                    lieupro[i]['contruiction']['ville'],
                    lieupro[i]['contruiction']['nom_t'],
                    groupe[0],
                    groupe[1],
                    groupe[2],
                    groupe[3],
                    lieupro[i]['contruiction']['sol'],
                    lieupro[i]['contruiction']['ouverture'],
                    lieupro[i]['contruiction']['commentaire'],
                    lieupro[i]['contruiction']['url'],
                    lieupro[i]['contruiction']['urlwaze'],
                  );
                  Navigator.pushNamed(context, '/Profil_terrain');
                },
                child: Column(
                  children: [
                    Container(
                      height: 30,
                    ),
                    Container(
                      height: MediaQuery.of(context).size.width * 0.5,
                      margin: EdgeInsets.symmetric(horizontal: 10.0),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.black,
                          width: 1,
                        ),
                      ),
                      child: Column(
                        children: [
                          //image
                          Container(
                            height: MediaQuery.of(context).size.width * 0.4,
                            color: Colors.black12,
                            child: SizedBox(
                              child: PageView.builder(
                                controller: PageController(viewportFraction: 1),
                                itemCount: groupe.length,
                                itemBuilder:
                                    (BuildContext context, int itemIndex) {
                                  String image = "";
                                  switch (itemIndex) {
                                    case 0:
                                      {
                                        image = groupe[0];
                                      }
                                      break;
                                    case 1:
                                      {
                                        image = groupe[1];
                                      }
                                      break;
                                    case 2:
                                      {
                                        image = groupe[2];
                                      }
                                      break;
                                    case 3:
                                      {
                                        image = groupe[3];
                                      }
                                      break;
                                    default:
                                  }

                                  return Image.network(
                                    image,
                                    fit: BoxFit.fitWidth,
                                  );
                                },
                              ),
                            ),
                          ),
                          Divider(
                            height: 2,
                            color: Colors.black,
                          ),
                          Text(
                            lieupro[i]['contruiction']['nom'],
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 30),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            }),
      );
    });
  }
}
