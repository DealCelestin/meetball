import 'package:flutter/material.dart';
import 'package:meetballl/footer.dart';
import 'package:meetballl/main.dart';
import 'package:meetballl/models/Model_img.dart';
import 'package:meetballl/models/Model_match.dart';
import 'package:meetballl/models/Model_terrain.dart';
import 'package:photo_view/photo_view.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:url_launcher/url_launcher.dart';

class Profil_terrain extends StatefulWidget {
  @override
  _Profil_terrainState createState() => _Profil_terrainState();
}

class _Profil_terrainState extends State<Profil_terrain> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: back,
      appBar: AppBar(
        centerTitle: true,
        title: Text("Rechercher", textAlign: TextAlign.center),
        backgroundColor: Colors.indigo,
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.directions),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                        title: Text('Ouvrir avec'),
                        content: SingleChildScrollView(
                            child: ListBody(children: <Widget>[
                          GestureDetector(
                            child: Text("Google map"),
                            onTap: () async {
                              String value =
                                  ScopedModel.of<TerrainModel>(context)
                                      .terrain_selectionner[10]
                                      .toString();
                              //const url = const value;
                              if (await canLaunch(value)) {
                                await launch(value);
                              }
                            },
                          ),
                          Padding(padding: EdgeInsets.all(8.0)),
                          GestureDetector(
                            child: Text("Waze"),
                            onTap: () async {
                              String value =
                                  ScopedModel.of<TerrainModel>(context)
                                      .terrain_selectionner[11]
                                      .toString();
                              //const url = const value;
                              if (await canLaunch(value)) {
                                await launch(value);
                              }
                            },
                          )
                        ])));
                  });
            },
          ),
        ],
      ),
      persistentFooterButtons: <Widget>[
        Footer(),
      ],
      body: SingleChildScrollView(child: Center(child:
          ScopedModelDescendant<TerrainModel>(
              builder: (context, child, terrain) {
        return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              ScopedModelDescendant<ImgModel>(builder: (context, child, img) {
                int nombre_photo = 0;
                if (terrain.terrain_selectionner[4] != "") {
                  nombre_photo++;
                }
                if (terrain.terrain_selectionner[5] != "") {
                  nombre_photo++;
                }
                if (terrain.terrain_selectionner[6] != "") {
                  nombre_photo++;
                }
                if (terrain.terrain_selectionner[7] != "") {
                  nombre_photo++;
                }
                if (nombre_photo == 0) {
                  return Container();
                } else {
                  return Container(
                    color: Colors.transparent,
                    child: SizedBox(
                      height: MediaQuery.of(context).size.height / 1.5,
                      child: PageView.builder(
                        controller: PageController(viewportFraction: 1),
                        itemCount: nombre_photo,
                        itemBuilder: (BuildContext context, int itemIndex) {
                          String image = "";
                          switch (itemIndex) {
                            case 0:
                              {
                                image = terrain.terrain_selectionner[4];
                              }
                              break;
                            case 1:
                              {
                                image = terrain.terrain_selectionner[5];
                              }
                              break;
                            case 2:
                              {
                                image = terrain.terrain_selectionner[6];
                              }
                              break;
                            case 3:
                              {
                                image = terrain.terrain_selectionner[7];
                              }
                              break;
                            default:
                          }

                          if (image == "") {
                            return Container();
                          } else {
                            return GestureDetector(
                              onTap: () {
                                return showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return Container(
                                          child: GestureDetector(
                                              onTap: () {
                                                Navigator.of(context).pop();
                                              },
                                              child: PhotoView(
                                                imageProvider:
                                                    NetworkImage(image),
                                              )));
                                    });
                              },
                              child: Image.network(
                                image,
                                width: MediaQuery.of(context).size.width,
                              ),
                            );
                          }
                        },
                      ),
                    ),
                  );
                }
              }),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(terrain.terrain_selectionner[0],
                              softWrap: true,
                              style: Theme.of(context).textTheme.headline3),
                          Text(terrain.terrain_selectionner[1],
                              softWrap: true,
                              style: Theme.of(context).textTheme.headline3),
                          Text(terrain.terrain_selectionner[2],
                              softWrap: true,
                              style: Theme.of(context).textTheme.headline3),
                          Text(
                              terrain.terrain_selectionner[3] +
                                  " terrain(s) disponible(s)",
                              softWrap: true,
                              style: Theme.of(context).textTheme.headline3),
                          Text(terrain.terrain_selectionner[8],
                              softWrap: true,
                              style: Theme.of(context).textTheme.headline3),
                          Text(terrain.terrain_selectionner[9],
                              softWrap: true,
                              style: Theme.of(context).textTheme.headline3),
                          Text(terrain.terrain_selectionner[12],
                              softWrap: true,
                              style: Theme.of(context).textTheme.headline3),
                        ]),
                  ]),
              Center(
                child: RaisedButton(
                  onPressed: () async {
                    ScopedModel.of<GameModel>(context).terrainrencontre =
                        terrain.terrain_selectionner[0].toString();
                    Navigator.pushNamed(context, '/TerrainRencontre');
                  },
                  child: Text('Rencontre à venir'),
                ),
              )
            ]);
      }))),
    );
  }
}
