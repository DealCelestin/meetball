import 'package:flutter/material.dart';
import 'package:meetballl/footer.dart';
import 'package:meetballl/main.dart';
import 'package:meetballl/models/Model_terrain.dart';
import 'package:photo_view/photo_view.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:url_launcher/url_launcher.dart';

import '../models/Model_img.dart';
import '../models/Model_match.dart';

List lieupro = [];

class TerrainRecherche extends StatefulWidget {
  @override
  _TerrainRechercheState createState() => _TerrainRechercheState();
}

double lat2 = 0;
double lon2 = 0;
double lon1;
double lat1;
double ditanceCourt = 1000000;

class _TerrainRechercheState extends State<TerrainRecherche> {
  List lisTerrain = [];
  bool init = true;
  bool afficher = true;
  Widget build(BuildContext context) {
    if (init) {
      setState(() {
        // ignore: unnecessary_statements
        lisTerrain;
      });
      init = false;
    }

    terrainre(String terrainre) async {
      List contruction = [];
      if (ScopedModel.of<TerrainModel>(context).vaDataTerrain.isEmpty) {
        await ScopedModel.of<TerrainModel>(context).terrain();
      }

      lisTerrain.clear();
      if (terrainre.isEmpty) {
        // quand l'utilisateur viens d'appuyer mais qu'il n'a rien écrit on passe ici et on affiche tous
        lisTerrain = [];
      } else {
        // on vas regarder mot pare mot si on a des lettre on commun avec la recherche
        int plusG = 0;
        for (var i = 0;
            i < ScopedModel.of<TerrainModel>(context).tailleTerrain;
            i++) {
          int nombre = 0;
          nombre = comparestring(
              terrainre.toUpperCase(),
              ScopedModel.of<TerrainModel>(context)
                  .vaDataTerrain[i]['ville']
                  .toUpperCase());
          if (nombre > 0 && nombre > (plusG - 2)) {
            plusG = nombre;
            // ici le lieu doit être affiche il vas dans construction
            Map tkt = {
              'contruiction':
                  ScopedModel.of<TerrainModel>(context).vaDataTerrain[i],
              "nombre": nombre
            };
            contruction.add(tkt);
          }
        }
        int copie = contruction.length;
        // objatif classer les lieu dans l'ordre
        for (var i = 0; i < copie; i++) {
          int nombreplus = 0;
          int place;
          for (var n = 0; n < contruction.length; n++) {
            if (contruction[n]['nombre'] >= nombreplus) {
              nombreplus = contruction[n]['nombre'];
              place = n;
            }
          }
          if (nombreplus >= (plusG - 1)) {
            lisTerrain.add(contruction[place]['contruiction']);
          }
          contruction.removeAt(place);
        }
      }
      setState(() {
        // ignore: unnecessary_statements
        lisTerrain;
      });
      afficher = true;
    }

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Rechercher un playground"),
          backgroundColor: Colors.indigo,
        ),
        persistentFooterButtons: <Widget>[
          Footer(),
        ],
        backgroundColor: back,
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Divider(color: Colors.grey),
              TextFormField(
                autocorrect: true,
                cursorColor: Colors.black,
                style: Theme.of(context).textTheme.headline3,
                decoration: const InputDecoration(
                  hintText: 'Trouver un playground',
                  hintStyle: TextStyle(color: Colors.black),
                ),
                onChanged: (value) {
                  setState(() {
                    afficher = false;
                  });
                  terrainre(value);
                },
              ),
              Container(
                height: 30,
              ),
              afficher
                  ? ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: lisTerrain.length,
                      itemBuilder: (context, i) {
                        return GestureDetector(
                          onTap: () {
                            //on vas a la page profil du terrain et avant on selectionne le bon terrain

                            int inNombreTour = 0;
                            String lien1 = "";
                            String lien2 = "";
                            String lien3 = "";
                            String lien4 = "";
                            while (
                                ScopedModel.of<ImgModel>(context).inTailleImg >
                                    inNombreTour) {
                              if (lisTerrain[i]['id'] ==
                                  ScopedModel.of<ImgModel>(context)
                                      .vaDataImg[inNombreTour]["id_lieu"]) {
                                if (lien1 == "") {
                                  lien1 = ScopedModel.of<ImgModel>(context)
                                      .vaDataImg[inNombreTour]["lien"];
                                } else if (lien2 == "") {
                                  lien2 = ScopedModel.of<ImgModel>(context)
                                      .vaDataImg[inNombreTour]["lien"];
                                } else if (lien3 == "") {
                                  lien3 = ScopedModel.of<ImgModel>(context)
                                      .vaDataImg[inNombreTour]["lien"];
                                } else if (lien4 == "") {
                                  lien4 = ScopedModel.of<ImgModel>(context)
                                      .vaDataImg[inNombreTour]["lien"];
                                }
                              }
                              inNombreTour++;
                            }

                            ScopedModel.of<TerrainModel>(context)
                                .terrainselectionner(
                                    lisTerrain[i]['nom'],
                                    lisTerrain[i]['adresse'],
                                    lisTerrain[i]['ville'],
                                    lisTerrain[i]['nom_t'],
                                    lien1,
                                    lien2,
                                    lien3,
                                    lien4,
                                    lisTerrain[i]['sol'],
                                    lisTerrain[i]['ouverture'],
                                    lisTerrain[i]['commentaire'],
                                    lisTerrain[i]['url'],
                                    lisTerrain[i]['urlwaze']);
                            Navigator.pushNamed(context, '/Profil_terrain');
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height / 20,
                            child: Center(
                              child: Text(
                                  lisTerrain[i]['nom'] +
                                      " (" +
                                      lisTerrain[i]['ville'] +
                                      ')',
                                  softWrap: true,
                                  style: Theme.of(context).textTheme.headline3),
                            ),
                          ),
                        );
                      })
                  : CircularProgressIndicator(),
            ],
          ),
        ));
  }
}
