import 'dart:convert';

import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:meetballl/main.dart';
import 'package:photo_view/photo_view.dart';
import 'package:scoped_model/scoped_model.dart';
import 'models/Model_img.dart';
import 'models/Model_terrain.dart';
import 'models/Model_ville.dart';
import 'footer.dart';

class Ville extends StatefulWidget {
  @override
  _VilleState createState() => _VilleState();
}

class _VilleState extends State<Ville> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: back,
        appBar: AppBar(
          centerTitle: true,
          title: Text("Rechercher", textAlign: TextAlign.center),
          backgroundColor: Colors.indigo,
        ),
        persistentFooterButtons: <Widget>[
          Footer(),
        ],
        body: Center(
          child: Container(
            child: FutureBuilder<List>(
              future: ScopedModel.of<VilleModel>(context).lieu_ville(),
              builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                if (snapshot.hasData) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/Terrain');
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: ListView.builder(
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          itemCount: snapshot.data.length,
                          itemBuilder: (context, i) {
                            List image1 = ["", "", "", ""];
                            List groupe = [];
                            groupe = snapshot.data[i]['groupe'];
                            image1.clear();
                            for (var n = 0; n < groupe.length; n++) {
                              image1.add(groupe[n]['lien']);
                            }
                            image1.add("");
                            image1.add("");
                            image1.add("");
                            image1.add("");
                            return Column(
                              children: [
                                Container(
                                  height: 30,
                                ),
                                GestureDetector(
                                  onTap: () async {
                                    await ScopedModel.of<TerrainModel>(context)
                                        .terrainselectionner(
                                      snapshot.data[i]["nom"],
                                      snapshot.data[i]["adresse"],
                                      snapshot.data[i]["ville"],
                                      snapshot.data[i]["nom_t"],
                                      image1[0].toString(),
                                      image1[1].toString(),
                                      image1[2].toString(),
                                      image1[3].toString(),
                                      snapshot.data[i]["sol"],
                                      snapshot.data[i]["ouverture"],
                                      snapshot.data[i]["commentaire"],
                                      snapshot.data[i]["url"],
                                      snapshot.data[i]["urlwaze"],
                                    );
                                    Navigator.pushNamed(
                                        context, '/Profil_terrain');
                                  },
                                  child: Container(
                                    height:
                                        MediaQuery.of(context).size.width * 0.5,
                                    margin:
                                        EdgeInsets.symmetric(horizontal: 10.0),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black,
                                        width: 1,
                                      ),
                                    ),
                                    child: Column(
                                      children: [
                                        //image
                                        Container(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.4,
                                          color: Colors.black12,
                                          child: SizedBox(
                                            child: PageView.builder(
                                              controller: PageController(
                                                  viewportFraction: 1),
                                              itemCount: groupe.length,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int itemIndex) {
                                                String image = "";
                                                switch (itemIndex) {
                                                  case 0:
                                                    {
                                                      image = groupe[0]['lien'];
                                                    }
                                                    break;
                                                  case 1:
                                                    {
                                                      image = groupe[1]['lien'];
                                                    }
                                                    break;
                                                  case 2:
                                                    {
                                                      image = groupe[2]['lien'];
                                                    }
                                                    break;
                                                  case 3:
                                                    {
                                                      image = groupe[3]['lien'];
                                                    }
                                                    break;
                                                  default:
                                                }

                                                return Image.network(
                                                  image,
                                                  fit: BoxFit.cover,
                                                );
                                              },
                                            ),
                                          ),
                                        ),
                                        Divider(
                                          height: 2,
                                          color: Colors.black,
                                        ),
                                        Text(
                                          snapshot.data[i]["nom"],
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              fontSize: 25,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            );
                          }),
                    ),
                  );
                } else {
                  return Center(
                    child: SizedBox(
                      child: CircularProgressIndicator(),
                      width: 60,
                      height: 60,
                    ),
                  );
                }
              },
            ),
          ),
        ));
  }
}
