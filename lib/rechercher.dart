import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:meetballl/main.dart';
import 'package:meetballl/models/Model_img.dart';
import 'package:meetballl/models/Model_ville.dart';
import 'package:scoped_model/scoped_model.dart';
import 'footer.dart';
import 'models/Model_terrain.dart';
import 'package:http/http.dart' as http;
import 'models/Model_ville.dart';

class Rechercher extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<TerrainModel>(
        builder: (context, child, model) {
      return PresentationRechercher();
    });
  }
}

class PresentationRechercher extends StatefulWidget {
  @override
  _PresentationRechercherState createState() => _PresentationRechercherState();
}

class _PresentationRechercherState extends State<PresentationRechercher> {
  int inNombreTour = 0;

  String lien;

// retourne la liste des ville enregistrer
  Future<List> ville() async {
    var url = 'https://meetball.fr/www/get_ville.php';
    http.Response response = await http.get(url);
    var data = jsonDecode(response.body);
    return data;
  }

  @override
  Widget build(BuildContext context) {
    print("recherhce");
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.indigo,
        leading: IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Navigator.pushNamed(context, '/Ajout_match');
            }),
        // actions: <Widget>[
        //   IconButton(
        //     icon: const Icon(Icons.star),
        //     onPressed: () {
        //       Navigator.pushNamed(context, '/classement');
        //     },
        //   ),
        // ],
      ),
      persistentFooterButtons: <Widget>[
        Footer(),
      ],
      backgroundColor: back,
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.4,
                child: OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    primary: Colors.black,
                  ),
                  onPressed: () {
                    ScopedModel.of<ImgModel>(context).listImage();
                    if (ScopedModel.of<TerrainModel>(context)
                        .vaDataTerrain
                        .isEmpty) {
                      ScopedModel.of<TerrainModel>(context).terrain();
                    }
                    Navigator.pushNamed(context, '/Terrain');
                  },
                  child: Text(
                    "TERRAINS",
                    style: TextStyle(
                        fontSize: 20,
                        color: Theme.of(context).brightness == Brightness.dark
                            ? Colors.white
                            : Colors.black),
                  ),
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.4,
                child: OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    primary: Colors.black,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/ProfilRechercher');
                  },
                  child: Text(
                    "JOUEURS",
                    style: TextStyle(
                        fontSize: 20,
                        color: Theme.of(context).brightness == Brightness.dark
                            ? Colors.white
                            : Colors.black),
                  ),
                ),
              ),
            ],
          ),
          FutureBuilder<List>(
            future: ville(),
            builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
              if (snapshot.hasData) {
                return Expanded(
                  child: Container(
                    child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, i) {
                          bool valide = false;
                          if (snapshot.data[i]["disponible"] == "1") {
                            valide = true;
                          }
                          String nom = snapshot.data[i]["nom"].toString();
                          String communes =
                              snapshot.data[i]["communes"].toString();
                          String terrains =
                              snapshot.data[i]["nombre_terrain"].toString();

                          return GestureDetector(
                            onTap: () {
                              if (valide) {
                                ScopedModel.of<VilleModel>(context)
                                    .ville_choisi = nom;
                                Navigator.pushNamed(context, '/ville');
                              }
                            },
                            child: Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.25,
                                child: Stack(
                                  children: [
                                    Image.network(
                                      snapshot.data[i]["img"].toString(),
                                      width: MediaQuery.of(context).size.width,
                                      fit: BoxFit.cover,
                                    ),
                                    valide
                                        ? Container()
                                        : Center(
                                            child: new ClipRect(
                                              child: new BackdropFilter(
                                                filter: new ImageFilter.blur(
                                                    sigmaX: 5.0, sigmaY: 5.0),
                                                child: new Container(
                                                  decoration: new BoxDecoration(
                                                      color: Colors
                                                          .grey.shade200
                                                          .withOpacity(0.05)),
                                                  child: new Center(
                                                    child: new Text(
                                                        'Bientôt disponible',
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .display3),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          nom,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 30),
                                        ),
                                        Text(
                                          "$communes communes, $terrains terrains",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      ],
                                    ),
                                  ],
                                )),
                          );
                        }),
                  ),
                );
              } else {
                return Center(
                  child: SizedBox(
                    child: CircularProgressIndicator(),
                    width: 60,
                    height: 60,
                  ),
                );
              }
            },
          )
        ],
      ),
    );
  }
}
