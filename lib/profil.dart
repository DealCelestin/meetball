import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image_picker/image_picker.dart';
import 'package:photo_view/photo_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:scoped_model/scoped_model.dart';
import 'footer.dart';
import 'main.dart';
import 'models/Model_co.dart';
import 'models/Model_img.dart';
import 'models/Model_match.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';

var now = new DateTime.now();
bool rencontre = true;
bool chnagecouleur = false;

class Profil extends StatefulWidget {
  @override
  _ProfilState createState() => _ProfilState();
}

class _ProfilState extends State<Profil> {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<LoginModel>(builder: (context, child, model) {
      return Container(
        child: model.loging == false
            ? Scaffold(
                persistentFooterButtons: <Widget>[
                    Footer(),
                  ],
                backgroundColor: back,
                body: Center(
                  child: CircularProgressIndicator(),
                ))
            : model.boRetourProfil
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                        Center(
                            child: model.boFauxPseudo
                                ? Text("ton email est faux",
                                    style:
                                        Theme.of(context).textTheme.headline1)
                                : Text("ton password est faux",
                                    style:
                                        Theme.of(context).textTheme.headline1)),
                        Center(
                            child: RaisedButton(
                          onPressed: () {
                            ScopedModel.of<LoginModel>(context).deconnection();
                            Navigator.pushNamedAndRemoveUntil(
                                context, '/', (Route<dynamic> route) => false);
                          },
                          textColor: Colors.white,
                          padding: const EdgeInsets.all(0.0),
                          child: Container(
                            decoration: const BoxDecoration(
                              gradient: LinearGradient(
                                colors: <Color>[
                                  Color(0xFF0D47A1),
                                  Color(0xFF1976D2),
                                  Color(0xFF42A5F5),
                                ],
                              ),
                            ),
                            padding: const EdgeInsets.all(10.0),
                            child: const Text('nouveaux test',
                                style: TextStyle(fontSize: 20)),
                          ),
                        ))
                      ])
                : Presentation(),
      );
    });
  }
}

class Presentation extends StatefulWidget {
  @override
  _PresentationState createState() => _PresentationState();
}

class _PresentationState extends State<Presentation> {
  File image;

  String base64Image = "";

  bool affImage = true;
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  @override
  Widget build(BuildContext context) {
    void _onRefresh() async {
      ScopedModel.of<LoginModel>(context).connexion(
          ScopedModel.of<LoginModel>(context).email,
          ScopedModel.of<LoginModel>(context).password);

      _refreshController.refreshCompleted();
    }

    misAjour() {
      affImage = false;
    }

// gestion des modification des images profile
    Future<void> _choisirimage(BuildContext context) {
      bool aff = true;
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return aff
                  ? SingleChildScrollView(
                      child: ListBody(children: <Widget>[
                      GestureDetector(
                        child: Text("galerie"),
                        onTap: () async {
                          image = await ImagePicker.pickImage(
                              source: ImageSource.gallery);
                          setState(() {
                            aff = false;
                          });

                          List<int> imageBytes = image.readAsBytesSync();
                          List<int> imageBytescompress =
                              await FlutterImageCompress.compressWithList(
                            imageBytes,
                            minHeight: 1920,
                            minWidth: 1080,
                            quality: 96,
                            rotate: 0,
                          );

                          base64Image = base64Encode(imageBytescompress);
                          await ScopedModel.of<LoginModel>(context)
                              .changeImage(base64Image);

                          misAjour();
                          Navigator.of(context).pop();
                        },
                      ),
                      Padding(padding: EdgeInsets.all(8.0)),
                      GestureDetector(
                        child: Text("caméra"),
                        onTap: () async {
                          image = await ImagePicker.pickImage(
                              source: ImageSource.camera);
                          setState(() {
                            aff = false;
                          });

                          List<int> imageBytes = image.readAsBytesSync();
                          base64Image = base64Encode(imageBytes);
                          await ScopedModel.of<LoginModel>(context)
                              .changeImage(base64Image);

                          misAjour();
                          Navigator.of(context).pop();
                        },
                      )
                    ]))
                  : SingleChildScrollView(
                      child: ListBody(children: <Widget>[
                      Text(
                          "Merci de patienter le temps de l'envoie de ta photo",
                          softWrap: true,
                          style: Theme.of(context).textTheme.headline3),
                      Container(
                        height: MediaQuery.of(context).size.height / 3,
                        width: MediaQuery.of(context).size.width / 3,
                        child: CircularProgressIndicator(),
                      )
                    ]));
            }));
          });
    }

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(ScopedModel.of<LoginModel>(context).pseudo),
          backgroundColor: Colors.indigo,
          leading: IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                Navigator.pushNamed(context, '/Ajout_match');
              }),
          actions: <Widget>[
            IconButton(
              // icon: const Icon(Icons.settings),
              icon: const Icon(Icons.more_vert),

              onPressed: () {
                Navigator.pushNamed(context, '/parametre');
              },
            ),
          ],
        ),
        persistentFooterButtons: <Widget>[
          Footer(),
        ],
        backgroundColor: back,
        body: SmartRefresher(
            enablePullDown: true,
            header: WaterDropHeader(),
            controller: _refreshController,
            onRefresh: _onRefresh,
            child: ScopedModelDescendant<LoginModel>(
                builder: (context, child, model) {
              // ignore: non_constant_identifier_names
              changement_rencontre() {
                // changer les rencontres afficher entre les passer et les futures
                setState(() {
                  if (model.participationProilfuture) {
                    ScopedModel.of<LoginModel>(context)
                            .participationProilfuture =
                        !ScopedModel.of<LoginModel>(context)
                            .participationProilfuture;
                    ScopedModel.of<LoginModel>(context).ParticipationPr();
                  } else {
                    ScopedModel.of<LoginModel>(context)
                            .participationProilfuture =
                        !ScopedModel.of<LoginModel>(context)
                            .participationProilfuture;
                    ScopedModel.of<LoginModel>(context).ParticipationPr();
                  }
                });
              }

              if (model.participation.length == 0) {
                rencontre = false;
              } else {
                rencontre = true;
              }
// calcule de l'age
              // var ms = (new DateTime.now()).millisecondsSinceEpoch;
              // String ok = "}" + model.age + "/";
              // int jour = int.parse(ok.split('}')[1].split('-')[0]);
              // int mois = int.parse(ok.split('-')[1].split('-')[0]);

              // String placement = jour.toString() + '-' + mois.toString() + '-';
              // int ans = int.parse(ok.split(placement)[1].split('/')[0]);

              // var mst = new DateTime.utc(ans, mois, jour, 20, 18, 04)
              //     .millisecondsSinceEpoch;
              // double douAge = ((ms - mst) / (365 * 24 * 3600 * 1000));
              // int ageAnne = douAge.toInt();
              // affichage du profile photo + nom +....
              return Column(
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  // mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 10,
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.3,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                GestureDetector(
                                    // photo
                                    onTap: () {
                                      return showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return affImage
                                                ? Container(
                                                    child: GestureDetector(
                                                    onTap: () {
                                                      Navigator.of(context)
                                                          .pop();
                                                    },
                                                    child: PhotoView(
                                                      imageProvider:
                                                          NetworkImage(
                                                              model.img),
                                                    ),
                                                  ))
                                                : Container(
                                                    child: GestureDetector(
                                                        onTap: () {
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                        child: PhotoView(
                                                          imageProvider:
                                                              FileImage(image),
                                                        )));
                                          });
                                    },
                                    onLongPress: () {
                                      _choisirimage(context);
                                    },
                                    child: CircleAvatar(
                                      radius:
                                          (MediaQuery.of(context).size.width /
                                                  7) +
                                              5,
                                      backgroundColor: Colors.black,
                                      child: CircleAvatar(
                                        backgroundImage: affImage
                                            ? NetworkImage(model.img)
                                            : FileImage(image),
                                        radius:
                                            (MediaQuery.of(context).size.width /
                                                    7) +
                                                4,
                                        // MediaQuery.of(context).size.width / 6,
                                      ),
                                    )),
                                Column(
                                  children: [
                                    Text(
                                      "OVERALL",
                                      softWrap: true,
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      model.note_perso.toString(),
                                      softWrap: true,
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: Column(
                              children: [
                                Container(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Column(
                                      children: [
                                        Text(
                                          "?",
                                          style: TextStyle(fontSize: 13),
                                        ),
                                        Text(
                                          "Abonnés",
                                          style: TextStyle(fontSize: 13),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Text(
                                          model.nombre_match.toString(),
                                          style: TextStyle(fontSize: 13),
                                        ),
                                        Text(
                                          "Match",
                                          style: TextStyle(fontSize: 13),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Text(
                                          "?",
                                          style: TextStyle(fontSize: 13),
                                        ),
                                        Text(
                                          "Abonnements",
                                          style: TextStyle(fontSize: 13),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    // bouton modification et ...

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        RaisedButton(
                            child: Text('Modifier le profil'),
                            onPressed: () {
                              model.affmodif = true;
                              Navigator.pushNamed(context, '/modif');
                            }),
                        RaisedButton(
                            child: Text("organiser une rencontre"),
                            onPressed: () {
                              Navigator.pushNamed(context, '/Ajout_match');
                            }),
                      ],
                    ),
                    Divider(color: Colors.grey[300]),
                    // affichage des differente rencontre

                    //bouton pour choisir entre rencontre passer ou future
                    Row(
                      children: <Widget>[
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 2,
                          child: OutlinedButton(
                            style: OutlinedButton.styleFrom(
                              primary: Colors.black,
                            ),
                            onPressed: () {
                              ScopedModel.of<LoginModel>(context)
                                  .participationProilfuture = false;
                              ScopedModel.of<LoginModel>(context)
                                  .ParticipationPr();
                            },
                            child: Image.asset(
                              'img/rencontre_passer.png',
                              width: MediaQuery.of(context).size.width * 0.15,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 2,
                          child: OutlinedButton(
                            style: OutlinedButton.styleFrom(
                              primary: Colors.black,
                            ),
                            onPressed: () {
                              ScopedModel.of<LoginModel>(context)
                                  .participationProilfuture = true;
                              ScopedModel.of<LoginModel>(context)
                                  .ParticipationPr();
                            },
                            child: Image.asset(
                              'img/rencontre_future.png',
                              width: MediaQuery.of(context).size.width * 0.15,
                            ),
                          ),
                        ),
                      ],
                    ),
                    //affichage des rencontres sélectionner

                    rencontre
                        ? Expanded(
                            child: GestureDetector(
                              onHorizontalDragEnd: (details) {
                                changement_rencontre();
                              },
                              child: Container(
                                // color: Colors.grey,
                                child: ListView.builder(
                                    // physics: NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemCount: model.participation.length,
                                    itemBuilder: (context, i) {
                                      // calcule du temps avant le match
                                      var ms = (new DateTime.now())
                                          .millisecondsSinceEpoch;
                                      String ok = "}" +
                                          model.participation[i]['jour'] +
                                          "/";
                                      int jour = int.parse(
                                          ok.split('}')[1].split('-')[0]);
                                      int mois = int.parse(
                                          ok.split('-')[1].split('-')[0]);
                                      String placement = jour.toString() +
                                          '-' +
                                          mois.toString() +
                                          '-';
                                      int ans = int.parse(
                                          ok.split(placement)[1].split('/')[0]);

                                      var mst = new DateTime.utc(
                                              ans, mois, jour, 20, 18, 04)
                                          .millisecondsSinceEpoch;

                                      String tempsavantmatch = 'erreur';
                                      double tkt =
                                          ((mst - ms) / (24 * 3600 * 1000))
                                              .abs();

                                      if (ms >= mst) {
                                        // dans le passer

                                        tempsavantmatch = "Il y as " +
                                            tkt.toInt().toString() +
                                            " jour(s) à " +
                                            model.participation[i]['heure'];
                                      } else {
                                        //dans le future
                                        if (tkt.toInt() == 0) {
                                          tempsavantmatch = "aujoud'hui à " +
                                              model.participation[i]['heure'];
                                        } else if (1 <= tkt && tkt < 2) {
                                          tempsavantmatch = "demain à " +
                                              model.participation[i]['heure'];
                                        } else {
                                          tempsavantmatch = "Dans " +
                                              tkt.toInt().toString() +
                                              " jour(s) à " +
                                              model.participation[i]['heure'];
                                        }
                                      }
                                      if (chnagecouleur) {
                                        chnagecouleur = false;
                                      } else {
                                        chnagecouleur = true;
                                      }
                                      if (tkt >= 0) {
                                        return GestureDetector(
                                          onTap: () async {
                                            // on sélection la rencontre choisir
                                            ScopedModel.of<GameModel>(context)
                                                    .lieu =
                                                model.participation[i]['lieu'];
                                            ScopedModel.of<GameModel>(context)
                                                    .inIdRencontre =
                                                model.participation[i]
                                                    ['stirnIdrencontre'];
                                            ScopedModel.of<GameModel>(context)
                                                    .nombJoueur =
                                                int.parse(model.participation[i]
                                                        ['nom_j']
                                                    .toString());
                                            ScopedModel.of<GameModel>(context)
                                                    .daterencontre =
                                                model.participation[i]['jour'];
                                            ScopedModel.of<GameModel>(context)
                                                    .heurerencontre =
                                                model.participation[i]['heure'];
                                            ScopedModel.of<ImgModel>(context)
                                                .imageTerrainId(model
                                                    .participation[i]['lieu']);
                                            // on prepare les image terrain et commentaire pour la page profil rencontre
                                            ScopedModel.of<ImgModel>(context)
                                                .listImage();
                                            ScopedModel.of<GameModel>(context)
                                                .terrain();

                                            // ScopedModel.of<GameModel>(context)
                                            //     .Commentaire();

                                            await ScopedModel.of<LoginModel>(
                                                    context)
                                                .personnePropose(
                                                    model.participation[i]
                                                        ['stirnIdrencontre']);

                                            // await ScopedModel.of<LoginModel>(context).Personne_propose( model.participation[i]['stirnIdrencontre']);
                                            ScopedModel.of<GameModel>(context)
                                                .lisCommentaire
                                                .clear();
                                            ScopedModel.of<GameModel>(context)
                                                    .nombre =
                                                0; // sela premette de reconmmencer l'affichage
                                            await ScopedModel.of<GameModel>(
                                                    context)
                                                .commentaire();
                                            Navigator.pushNamed(
                                                context, '/Profil_renctontre');
                                          },
                                          child: Center(
                                              child: Column(
                                            children: [
                                              Container(
                                                  // margin:
                                                  //     const EdgeInsets.all(10),
                                                  child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: <Widget>[
                                                    Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceEvenly,
                                                        children: <Widget>[
                                                          Column(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              children: <
                                                                  Widget>[
                                                                Text(
                                                                  tempsavantmatch,
                                                                  softWrap:
                                                                      true,
                                                                  overflow:
                                                                      TextOverflow
                                                                          .visible,
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        16.0,
                                                                    color: Colors
                                                                        .black,
                                                                    decorationColor:
                                                                        Colors
                                                                            .white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w800,
                                                                    fontFamily:
                                                                        'Roboto',
                                                                    letterSpacing:
                                                                        0.5,
                                                                  ),
                                                                ),
                                                                Text(
                                                                  " au terrain " +
                                                                      model.participation[
                                                                              i]
                                                                          [
                                                                          'lieu'],
                                                                  softWrap:
                                                                      true,
                                                                  overflow:
                                                                      TextOverflow
                                                                          .visible,
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        16.0,
                                                                    color: Colors
                                                                        .black,
                                                                    decorationColor:
                                                                        Colors
                                                                            .white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w800,
                                                                    fontFamily:
                                                                        'Roboto',
                                                                    letterSpacing:
                                                                        0.5,
                                                                  ),
                                                                ),
                                                                Text(
                                                                  "Il y a " +
                                                                      model.participation[
                                                                              i]
                                                                          [
                                                                          'nom_j'] +
                                                                      " personne(s)",
                                                                  softWrap:
                                                                      true,
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        16.0,
                                                                    color: Colors
                                                                        .black,
                                                                    decorationColor:
                                                                        Colors
                                                                            .white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w800,
                                                                    fontFamily:
                                                                        'Roboto',
                                                                    letterSpacing:
                                                                        0.5,
                                                                  ),
                                                                ),
                                                              ]),
                                                        ]),
                                                  ])),
                                              Divider(color: Colors.black),
                                            ],
                                          )),
                                        );
                                      } else {
                                        return Container();
                                      }
                                    }),
                              ),
                            ),
                          )
                        : Expanded(
                            child: GestureDetector(
                                onHorizontalDragEnd: (details) {
                                  changement_rencontre();
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  color: Colors.grey,
                                  child: Center(
                                    child: Text("tu n'a pas de rencontre ",
                                        softWrap: true,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline3),
                                  ),
                                ))),
                  ]);
            })));
  }
}
