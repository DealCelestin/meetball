import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:meetballl/models/Model_match.dart';
import 'package:photo_view/photo_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:scoped_model/scoped_model.dart';
import 'footer.dart';
import 'main.dart';
import 'models/Model_co.dart';
import 'models/Model_img.dart';

var now = new DateTime.now();
bool rencontre = true;
bool chnagecouleur = false;

class ProfilVisiteur extends StatefulWidget {
  @override
  _ProfilVisiteurState createState() => _ProfilVisiteurState();
}

class _ProfilVisiteurState extends State<ProfilVisiteur> {
  @override
  Widget build(BuildContext context) {
    RefreshController _refreshController =
        RefreshController(initialRefresh: false);
    void _onRefresh() async {
      ScopedModel.of<LoginModel>(context).ParticipationProilVisiteur(
          ScopedModel.of<LoginModel>(context).profVisiteur["pseudo"]);

      _refreshController.refreshCompleted();
    }

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title:
              Text(ScopedModel.of<LoginModel>(context).profVisiteur["pseudo"]),
          backgroundColor: Colors.indigo,
        ),
        persistentFooterButtons: <Widget>[
          Footer(),
        ],
        backgroundColor: back,
        body: SmartRefresher(
            enablePullDown: true,
            header: WaterDropHeader(),
            controller: _refreshController,
            onRefresh: _onRefresh,
            child: ScopedModelDescendant<LoginModel>(
                builder: (context, child, model) {
              if (model.participation.length == 0) {
                rencontre = false;
              } else {
                rencontre = true;
              }
// calcule de l'age
              var ms = (new DateTime.now()).millisecondsSinceEpoch;
              String ok = "}" + model.profVisiteur["age"] + "/";
              int jour = int.parse(ok.split('}')[1].split('-')[0]);
              int mois = int.parse(ok.split('-')[1].split('-')[0]);

              String placement = jour.toString() + '-' + mois.toString() + '-';
              int ans = int.parse(ok.split(placement)[1].split('/')[0]);

              var mst = new DateTime.utc(ans, mois, jour, 20, 18, 04)
                  .millisecondsSinceEpoch;
              double douAge = ((ms - mst) / (365 * 24 * 3600 * 1000));
              int ageAnne = douAge.toInt();
              return Column(
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  // mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 10,
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.3,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              GestureDetector(
                                  // photo
                                  onTap: () {
                                    return showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return Container(
                                              child: GestureDetector(
                                                  onTap: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: PhotoView(
                                                    imageProvider: NetworkImage(
                                                        model.profVisiteur[
                                                            "photo"]),
                                                  )));
                                        });
                                  },
                                  child: CircleAvatar(
                                    radius: (MediaQuery.of(context).size.width /
                                            7) +
                                        5,
                                    backgroundColor: Colors.black,
                                    child: CircleAvatar(
                                      backgroundImage: NetworkImage(
                                          model.profVisiteur["photo"]),
                                      radius:
                                          (MediaQuery.of(context).size.width /
                                                  7) +
                                              4,
                                      // MediaQuery.of(context).size.width / 6,
                                    ),
                                  )),
                              Column(
                                children: [
                                  Text(
                                    "OVERALL",
                                    softWrap: true,
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    model.profVisiteur["note"],
                                    softWrap: true,
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width * 0.6,
                                child: Column(
                                  children: [
                                    Container(
                                      height: 20,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        Column(
                                          children: [
                                            Text(
                                              "?",
                                              style: TextStyle(fontSize: 13),
                                            ),
                                            Text(
                                              "Abonnés",
                                              style: TextStyle(fontSize: 13),
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: [
                                            Text(
                                              model.profVisiteur["nombre_match"]
                                                  .toString(),
                                              style: TextStyle(fontSize: 13),
                                            ),
                                            Text(
                                              "Match",
                                              style: TextStyle(fontSize: 13),
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: [
                                            Text(
                                              "?",
                                              style: TextStyle(fontSize: 13),
                                            ),
                                            Text(
                                              "Abonnements",
                                              style: TextStyle(fontSize: 13),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                height: 10,
                              ),
                              Container(
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Row(
                                                    children: <Widget>[
                                                      Text(
                                                          model.profVisiteur[
                                                                  "prenom"] +
                                                              " ",
                                                          softWrap: true,
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .headline3),
                                                      Text(
                                                          model.profVisiteur[
                                                              "nom"],
                                                          softWrap: true,
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .headline3),
                                                    ],
                                                  ),
                                                  Text(
                                                      ageAnne.toString() +
                                                          " ans",
                                                      softWrap: true,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .headline3),
                                                  Text(
                                                      model
                                                          .profVisiteur["club"],
                                                      softWrap: true,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .headline3),
                                                  Text(
                                                      model.profVisiteur[
                                                          "niveaux"],
                                                      softWrap: true,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .headline3),
                                                  Container(
                                                    height: 10,
                                                  ),
                                                  Text(
                                                      model.profVisiteur[
                                                          "message"],
                                                      softWrap: true,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .headline3),
                                                ]),
                                          ],
                                        ),
                                      ),
                                    ]),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        RaisedButton(
                            child: Text('Envoyer une invitation'),
                            onPressed: () {
                              Scaffold.of(context).showSnackBar(new SnackBar(
                                  content: new Text(
                                      "Cette fonctionnalite n'existe pas encore")));
                            }),
                      ],
                    ),
                    Divider(color: Colors.grey[300]),

                    //bouton pour choisir entre rencontre passer ou future
                    Row(
                      children: <Widget>[
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 2,
                          child: OutlinedButton(
                            style: OutlinedButton.styleFrom(
                              primary: Colors.black,
                            ),
                            onPressed: () {
                              ScopedModel.of<LoginModel>(context)
                                  .participationProilfutureVisiteur = false;
                              ScopedModel.of<LoginModel>(context)
                                  .ParticipationProilVisiteur(
                                      model.profVisiteur["pseudo"]);
                            },
                            child: Image.asset(
                              'img/rencontre_passer.png',
                              width: MediaQuery.of(context).size.width * 0.15,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 2,
                          child: OutlinedButton(
                            style: OutlinedButton.styleFrom(
                              primary: Colors.black,
                            ),
                            onPressed: () {
                              ScopedModel.of<LoginModel>(context)
                                  .participationProilfutureVisiteur = true;
                              ScopedModel.of<LoginModel>(context)
                                  .ParticipationProilVisiteur(
                                      model.profVisiteur["pseudo"]);
                            },
                            child: Image.asset(
                              'img/rencontre_future.png',
                              width: MediaQuery.of(context).size.width * 0.15,
                            ),
                          ),
                        ),
                      ],
                    ),
                    rencontre
                        ? Expanded(
                            child: GestureDetector(
                              onHorizontalDragEnd: (details) {
                                // setState(() {
                                //   model.participationProilfutureVisiteur =
                                //       !model.participationProilfutureVisiteur;
                                // });
                                setState(() {
                                  if (model.participationProilfutureVisiteur) {
                                    ScopedModel.of<LoginModel>(context)
                                            .participationProilfutureVisiteur =
                                        false;
                                    ScopedModel.of<LoginModel>(context)
                                        .ParticipationProilVisiteur(
                                            model.profVisiteur["pseudo"]);
                                  } else {
                                    ScopedModel.of<LoginModel>(context)
                                            .participationProilfutureVisiteur =
                                        true;
                                    ScopedModel.of<LoginModel>(context)
                                        .ParticipationProilVisiteur(
                                            model.profVisiteur["pseudo"]);
                                  }
                                });
                              },
                              child: Container(
                                child: ListView.builder(
                                    // physics: NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemCount:
                                        model.participationvisiteur.length,
                                    itemBuilder: (context, i) {
                                      // calcule du temps avant le match
                                      var ms = (new DateTime.now())
                                          .millisecondsSinceEpoch;
                                      String ok = "}" +
                                          model.participationvisiteur[i]
                                              ['jour'] +
                                          "/";
                                      int jour = int.parse(
                                          ok.split('}')[1].split('-')[0]);
                                      int mois = int.parse(
                                          ok.split('-')[1].split('-')[0]);
                                      String placement = jour.toString() +
                                          '-' +
                                          mois.toString() +
                                          '-';
                                      int ans = int.parse(
                                          ok.split(placement)[1].split('/')[0]);

                                      var mst = new DateTime.utc(
                                              ans, mois, jour, 20, 18, 04)
                                          .millisecondsSinceEpoch;

                                      String tempsavantmatch = 'bnokt';
                                      double tkt =
                                          ((mst - ms) / (24 * 3600 * 1000))
                                              .abs();

                                      if (ms >= mst) {
                                        // dans le passer

                                        tempsavantmatch = "il y as " +
                                            tkt.toInt().toString() +
                                            " jour(s) à " +
                                            model.participationvisiteur[i]
                                                ['heure'];
                                      } else {
                                        //dans le future
                                        if (tkt.toInt() == 0) {
                                          tempsavantmatch = "aujoud'hui à " +
                                              model.participationvisiteur[i]
                                                  ['heure'];
                                        } else if (1 <= tkt && tkt < 2) {
                                          tempsavantmatch = "demain à " +
                                              model.participationvisiteur[i]
                                                  ['heure'];
                                        } else {
                                          tempsavantmatch = "dans " +
                                              tkt.toInt().toString() +
                                              " jour(s) à " +
                                              model.participationvisiteur[i]
                                                  ['heure'];
                                        }
                                      }
                                      if (chnagecouleur) {
                                        chnagecouleur = false;
                                      } else {
                                        chnagecouleur = true;
                                      }
                                      if (tkt >= 0) {
                                        return GestureDetector(
                                          onTap: () async {
                                            // on sélection la rencontre choisir

                                            ScopedModel.of<GameModel>(context)
                                                    .lieu =
                                                model.participationvisiteur[i]
                                                    ['lieu'];
                                            ScopedModel.of<GameModel>(context)
                                                    .inIdRencontre =
                                                model.participationvisiteur[i]
                                                    ['stirnIdrencontre'];
                                            ScopedModel.of<GameModel>(context)
                                                    .nombJoueur =
                                                int.parse(model
                                                    .participationvisiteur[i]
                                                        ['nom_j']
                                                    .toString());
                                            ScopedModel.of<GameModel>(context)
                                                    .daterencontre =
                                                model.participationvisiteur[i]
                                                    ['jour'];
                                            ScopedModel.of<GameModel>(context)
                                                    .heurerencontre =
                                                model.participationvisiteur[i]
                                                    ['heure'];
                                            ScopedModel.of<ImgModel>(context)
                                                .imageTerrainId(
                                                    model.participationvisiteur[
                                                        i]['lieu']);

                                            // on prepare les image terrain et commentaire pour la page profil rencontre
                                            ScopedModel.of<ImgModel>(context)
                                                .listImage();
                                            ScopedModel.of<GameModel>(context)
                                                .terrain();

                                            // ScopedModel.of<GameModel>(context)
                                            //     .Commentaire();

                                            await ScopedModel.of<LoginModel>(
                                                    context)
                                                .personnePropose(
                                                    model.participationvisiteur[
                                                        i]['stirnIdrencontre']);

                                            // await ScopedModel.of<LoginModel>(context).Personne_propose( model.participation[i]['stirnIdrencontre']);
                                            ScopedModel.of<GameModel>(context)
                                                .lisCommentaire
                                                .clear();
                                            ScopedModel.of<GameModel>(context)
                                                    .nombre =
                                                0; // sela premette de reconmmencer l'affichage
                                            await ScopedModel.of<GameModel>(
                                                    context)
                                                .commentaire();
                                            Navigator.pushNamed(
                                                context, '/Profil_renctontre');
                                          },
                                          child: Center(
                                              child: Column(
                                            children: [
                                              Container(
                                                  // margin:
                                                  //     const EdgeInsets.all(10),
                                                  child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: <Widget>[
                                                    Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceEvenly,
                                                        children: <Widget>[
                                                          Column(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              children: <
                                                                  Widget>[
                                                                Text(
                                                                  tempsavantmatch,
                                                                  softWrap:
                                                                      true,
                                                                  overflow:
                                                                      TextOverflow
                                                                          .visible,
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        16.0,
                                                                    color: Colors
                                                                        .black,
                                                                    decorationColor:
                                                                        Colors
                                                                            .white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w800,
                                                                    fontFamily:
                                                                        'Roboto',
                                                                    letterSpacing:
                                                                        0.5,
                                                                  ),
                                                                ),
                                                                Text(
                                                                  " au terrain " +
                                                                      model.participation[
                                                                              i]
                                                                          [
                                                                          'lieu'],
                                                                  softWrap:
                                                                      true,
                                                                  overflow:
                                                                      TextOverflow
                                                                          .visible,
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        16.0,
                                                                    color: Colors
                                                                        .black,
                                                                    decorationColor:
                                                                        Colors
                                                                            .white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w800,
                                                                    fontFamily:
                                                                        'Roboto',
                                                                    letterSpacing:
                                                                        0.5,
                                                                  ),
                                                                ),
                                                                Text(
                                                                  "Il y a " +
                                                                      model.participation[
                                                                              i]
                                                                          [
                                                                          'nom_j'] +
                                                                      " personne(s)",
                                                                  softWrap:
                                                                      true,
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        16.0,
                                                                    color: Colors
                                                                        .black,
                                                                    decorationColor:
                                                                        Colors
                                                                            .white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w800,
                                                                    fontFamily:
                                                                        'Roboto',
                                                                    letterSpacing:
                                                                        0.5,
                                                                  ),
                                                                ),
                                                              ]),
                                                        ]),
                                                  ])),
                                              Divider(color: Colors.black),
                                            ],
                                          )),
                                        );
                                      } else {
                                        return Container();
                                      }
                                    }),
                              ),
                            ),
                          )
                        : Center(
                            child: Text("tu n'a pas de rencontre de prevue",
                                softWrap: true,
                                style: Theme.of(context).textTheme.headline3)),
                  ]);
            })));
  }
}
