import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server/gmail.dart';
import 'package:scoped_model/scoped_model.dart';
import 'footer.dart';
import 'models/Model_co.dart';

String avis;
var _controller = TextEditingController();

class Avis extends StatefulWidget {
  @override
  _AvisState createState() => _AvisState();
}

class _AvisState extends State<Avis> {
  String pseudo;

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Avis"),
          backgroundColor: Colors.indigo,
        ),
        persistentFooterButtons: <Widget>[
          Footer(),
        ],
        // backgroundColor: Colors.black,
        body: Column(mainAxisAlignment: MainAxisAlignment.center, children: <
            Widget>[
          Center(
            child: Text("Qu'aimerais-tu avoir de plus sur cette application?",
                style: Theme.of(context).textTheme.headline3),
          ),
          Center(
            child: Text("N'hésites pas à nous donner des idées ci-dessous !",
                style: Theme.of(context).textTheme.headline3),
          ),
          Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextFormField(
                        autocorrect: true,
                        controller: _controller,
                        maxLines: 5,
                        cursorColor: Colors.black,
                        style: TextStyle(
                            color: Colors.black, decorationColor: Colors.black),
                        decoration: const InputDecoration(
                          hintStyle: TextStyle(color: Colors.black),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                        onChanged: (value) {
                          avis = value;
                        },
                      ),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: RaisedButton(
                            onPressed: () async {
                              if (_formKey.currentState.validate()) {
                                pseudo =
                                    ScopedModel.of<LoginModel>(context).pseudo;
                                envoieAvis(pseudo, context);
                                setState(() {
                                  _controller.clear();
                                });
                                // ignore: deprecated_member_use
                                Scaffold.of(context).showSnackBar(new SnackBar(
                                    content: new Text('Ton avis est envoyer')));
                              }
                            },
                            child: Text('Envoyer'),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ])
        ]));
  }
}

Future<String> envoieAvis(String pseudo, BuildContext context) async {
  String json = '{"avis":"$avis","pseudo":"$pseudo"}';
  String url = 'https://meetball.fr/www/post_avis.php';
  // make POST request
  Response response = await post(url, body: json);
  String body = response.body;
  String mail = ScopedModel.of<LoginModel>(context).email;
  String prenom = ScopedModel.of<LoginModel>(context).prenom;

  String username = 'equipemeetball@gmail.com';
  String password = 'Projet1*';

  // ignore: deprecated_member_use
  final smtpServer = gmail(username, password);
  // Use the SmtpServer class to configure an SMTP server:
  // final smtpServer = SmtpServer('smtp.domain.com');
  // See the named arguments of SmtpServer for further configuration
  // options.

  // Create our message.
  final message = Message()
    ..from = Address(username, 'équipe Meetball')
    ..recipients.add('equipemeetball@gmail.com')
    // ..bccRecipients.add(Address('bccAddress@example.com'))
    ..subject = 'Nouvelle avis'
    ..text = 'This is the plain text.\nThis is line 2 of the text part.'
    ..html =
        "<h1>Nouvelle avis </h1>\n<p> $pseudo </p>\n<p> $mail </p>\n<p>  $avis </p>";
  await send(message, smtpServer);

  // Use the SmtpServer class to configure an SMTP server:
  // final smtpServer = SmtpServer('smtp.domain.com');
  // See the named arguments of SmtpServer for further configuration
  // options.

  // Create our message.
  final message2 = Message()
    ..from = Address(username, 'équipe Meetball')
    ..recipients.add(mail)
    // ..bccRecipients.add(Address('bccAddress@example.com'))
    ..subject = 'Nouvelle avis Meetball'
    ..html =
        "<h1>Bonjour $prenom</h1>\n<p>Votre commentaire a bien été pris en considération, après lecture par les équipes Meetball, un conseiller viendra vers vous si nécessaire. </p>\n<p> A très bientôt sur les Playground. </p>\n<p>  Chloé de Meetball </p>";
  await send(message2, smtpServer);

  return body;
}
