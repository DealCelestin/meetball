import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server/gmail.dart';
import 'package:meetballl/main.dart';
import 'package:meetballl/models/Model_co.dart';
import 'package:scoped_model/scoped_model.dart';

class Password extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    email(String messagee, String email) async {
      // var options = new GmailSmtpOptions()
      //   ..username = 'equipemeetball@gmail.com'
      //   ..password = 'Projet1*';
      // var emailTransport = new SmtpTransport(options);

      // // Create our mail/envelope.
      // var envelope = new Envelope()
      //   ..from = '$email'
      //   ..recipients.add('$email')
      //   ..subject = 'Nouveaux mots de passe'
      //   ..text = 'Ton nouveau mots de passe est $message';
      // emailTransport.send(envelope);

      String username = 'equipemeetball@gmail.com';
      String password = 'Projet1*';

      // ignore: deprecated_member_use
      final smtpServer = gmail(username, password);
      // Use the SmtpServer class to configure an SMTP server:
      // final smtpServer = SmtpServer('smtp.domain.com');
      // See the named arguments of SmtpServer for further configuration
      // options.

      // Create our message.
      final message = Message()
        ..from = Address(username, 'équipe Meetball')
        ..recipients.add(email)
        // ..bccRecipients.add(Address('bccAddress@example.com'))
        ..subject = 'Nouveaux mots de passe'
        ..text = 'This is the plain text.\nThis is line 2 of the text part.'
        ..html =
            "<h1>Nouveaux mots de passe </h1>\n<p> Ton nouveau mots de passe est $messagee </p>";
      await send(message, smtpServer);
    }

    String strinEmailChange;
    return Scaffold(
        backgroundColor: back,
        body:
            ScopedModelDescendant<LoginModel>(builder: (context, child, model) {
          return Stack(
            children: [
              Container(
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage('img/font.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextFormField(
                    autocorrect: true,
                    decoration: InputDecoration(
                      filled: false,
                      fillColor: Colors.black,
                      hintText: 'adresse email',
                      hintStyle: TextStyle(color: Colors.black),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return "entrer une adresse email";
                      }
                      return null;
                    },
                    onChanged: (value) {
                      strinEmailChange = value;
                    },
                  ),
                  RaisedButton(
                      onPressed: () async {
                        // vérification de l'email pour s'avoir si il fait bien partir de notre basse de donner
                        String url =
                            'https://meetball.fr/www/post_connexion.php'; // vérification email
                        String json = '{"email":"$strinEmailChange"}';
                        Response response = await post(url, body: json);
                        List listpersonne = jsonDecode(response.body);
                        if (listpersonne.isNotEmpty) {
                          String url =
                              'https://meetball.fr/www/post_password.php';
                          String json = '{"email":"$strinEmailChange"}';
                          Response response = await post(url, body: json);
                          String body = response.body;
                          email(body, strinEmailChange);
                          Scaffold.of(context).showSnackBar(new SnackBar(
                              content: new Text(
                                  "Le nouveau mots de passe est envoyer à l'adresse mail")));
                        } else {
                          Scaffold.of(context).showSnackBar(
                            SnackBar(
                              content: Text("Cette email n'exist pas "),
                            ),
                          );
                        }
                      },
                      child: Text(
                        'Ressevoir un nouveaux mots de passe',
                        textAlign: TextAlign.center,
                      )),
                ],
              ),
            ],
          );
        }));
  }
}
